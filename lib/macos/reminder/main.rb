# frozen_string_literal: true

require 'posix/spawn'

module MacOS
  class Reminder
    attr_reader :comment, :due_date, :list, :task, :reminder_id, :x_callback_url

    def initialize(comment: nil, due_date: nil, list: 'Reminders', task:)
      @comment = comment
      @due_date = due_date
      @list = list
      @task = task

      @reminder_id = nil
      @rv_osascript = nil
      @x_callback_url = nil
    end

    def create
      @rv_osascript ||=
        POSIX::Spawn::Child.new('/usr/bin/osascript', '-e', build_applescript)

      if @rv_osascript.success?
        @x_callback_url =
          @rv_osascript.out.strip.gsub(%r{^.* (.+?:\/\/.+)$}, '\1')
        @reminder_id = @x_callback_url.split('//').last
      end

      @rv_osascript.success?
    end

    def success?
      @rv_osascript ? @rv_osascript.success? : nil
    end

    private

    def build_applescript
      <<~EOTXT
        set listName to "#{@list}"
        set dueDate to "#{
        @due_date
      }"
        set task to "#{@task}"
        set comment to "#{
        @comment
      }"

        tell application "Reminders"
          if not (exists list listName) then
            make new list with properties {name:listName}
          end if

          if (dueDate is not "") then
            set theDate to current date
            tell theDate to set {its year, its month, its day} to words of dueDate

            return make new reminder ¬
              with properties {name:task, body:comment, allday due date:theDate} ¬
              at list listName
          end if

          return make new reminder ¬
            with properties {name:task, body:comment} at list listName
        end tell
      EOTXT
    end
  end
end
