# frozen_string_literal: true

Gem::Specification.new do |spec|
  repo_url = 'https://gitlab.com/czottmann/macos-reminder'

  spec.name = 'macos-reminder'
  spec.version = '1.0.0'
  spec.authors = ['Carlo Zottmann']
  spec.email = %w[carlo@zottmann.org]

  spec.summary = 'Simple osascript wrapper for creating macOS reminders.'
  spec.homepage = repo_url
  spec.license = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.3.0')

  # spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"

  spec.metadata['homepage_uri'] = repo_url
  spec.metadata['source_code_uri'] = repo_url
  spec.metadata['changelog_uri'] = "#{repo_url}/-/blob/main/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added
  # into git.
  spec.files =
    Dir.chdir(File.expand_path('..', __FILE__)) do
      `git ls-files -z`.split("\x0").reject do |f|
        f.match(%r{^(test|spec|features)/})
      end
    end
  spec.require_paths = %w[lib]

  spec.add_dependency 'posix-spawn', '~> 0.3.15'
  spec.add_development_dependency 'pry', '~> 0.13.1'
  spec.add_development_dependency 'rake', '~> 12.0'
end
